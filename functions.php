<?php

add_action( 'wp_enqueue_scripts', 'add_child_theme_stylesheets', PHP_INT_MAX );
function add_child_theme_stylesheets() {
    $wptheme = wp_get_theme();
    $theme_version = $wptheme->version;
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );  	 	
 	wp_enqueue_style( 'hicaliber-child-style', get_stylesheet_directory_uri() . '/assets/css/hicaliber-child-theme.css', array(), $theme_version, '', 'all');
	//wp_enqueue_script( 'hicaliber-child-script', get_stylesheet_directory_uri() . '/assets/js/hicaliber-child-theme.js', array('jquery'), $theme_version, true );
}